# Relearning Django

With Django 3.0, I thought I would take some time to learn how to write a Django application right.

I will be using some Cookiecutter templates and learn what the thought is behind them. From there, I might build
 something like a simple blog or an events site. Whatever comes to mind when I am ready to start designing something. 
 
 For now, I am just going to be reviewing the files, making a few adjustments, and then go from there.
 
